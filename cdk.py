#!/usr/bin/env python3
import aws_cdk as cdk
from stack import CdkStack
from cdk_env import get_settings

settings = get_settings()
app = cdk.App()
CdkStack(app, settings.project_name)

app.synth()