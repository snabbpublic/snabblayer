from aws_cdk import (
    Stack,
    aws_lambda as _lambda,
    aws_lambda_python_alpha as python,
    CfnOutput as Output
)
from constructs import Construct

class CdkStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        def res_name(name):
            return f"{construct_id}-{name}"

        lambda_layer = python.PythonLayerVersion(
            self, res_name("LambdaLayer"),
            entry="SnabbLayer",
            compatible_runtimes=[_lambda.Runtime.PYTHON_3_9]
        )

        Output(
            self, res_name("LambdaLayerArn"),
            value=lambda_layer.layer_version_arn
        )