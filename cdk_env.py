from functools import lru_cache
from pydantic import BaseSettings

class Settings(BaseSettings):
    project_name: str = "SnabbLayer"

@lru_cache()
def get_settings():
    return Settings()