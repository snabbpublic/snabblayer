import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="snabb-layer",
    version="0.0.1",
    author="Cristobal Aguila",
    author_email="developers@snabb.cl",
    description="Multi tenant and logging managment for Snabb FastAPI Projects",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/snabbpublic/snabblayer",
    packages=['layers'],
    package_dir={'layers': 'SnabbLayer/layers'},
    install_requires=[
        'fastapi',
        'boto3',
        'aws-lambda-powertools[Tracer,Logger,Metrics]',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)