
# Snabb Lambda Layer CDK

## How to use
This project is intended to work as a lambda layer to be used within Snabb FastAPI Microservices. Simplifies multi tenant managment.
It uses cdk to be deployed on AWS.

To use this project in development env include the repo in the requirements-dev.txt file

```
git+https://gitlab.com/snabbpublic/snabblayer.git
```

or install it directly with pip inside a virtualenv

```
$ pip install git+https://gitlab.com/snabbpublic/snabblayer.git
```

## Libraries

This layer contains the following libraries:
- logger.py
    - Util to log to aws CloudWatch
- metrics_manager.py
    - Util to send metrics to aws CloudWatch
- models.py
    - Tenant models
- tenant_dal.py
    - Database Layer for Tenants.
- tenant_manager.py
    - Adds tenant creation route to fastAPI.
    - Adds authorization method to fastAPI.
## Deployment

The `cdk.json` file tells the CDK Toolkit how to execute your app.

To manually create a virtualenv on MacOS and Linux:

```
$ python3 -m venv .env
```

After the init process completes and the virtualenv is created, you can use the following
step to activate your virtualenv.

```
$ source .env/bin/activate
```

If you are a Windows platform, you would activate the virtualenv like this:

```
% .venv\Scripts\activate.bat
```

Once the virtualenv is activated, you can install the required dependencies.

```
$ pip install -r requirements.txt
```

At this point you can now synthesize the CloudFormation template for this code.

```
$ cdk synth
```

Then you must deploy de CloudFormation template.

```
$ cdk deploy
```
## Useful commands

 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation
