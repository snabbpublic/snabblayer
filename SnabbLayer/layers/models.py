from pydantic import BaseModel
from typing import Optional

class Tenant(BaseModel):
    tenant_id: str
    name: str
    api_key: str
    scope: list

class TenantRequest(BaseModel):
    name: str
    scope: Optional[list] = ["*"]

class ExistingTenantException(Exception):
    pass

class TenantNotFoundException(Exception):
    pass

class TenantCreationException(Exception):
    pass
