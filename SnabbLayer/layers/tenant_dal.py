from uuid import uuid4
import boto3
from boto3.dynamodb.conditions import Key
from boto3.dynamodb.types import TypeDeserializer
from botocore.exceptions import ClientError
from .models import Tenant, ExistingTenantException, TenantCreationException, TenantNotFoundException
import os

table_name = f"tenant-{os.environ.get('project_name')}"

session = boto3.Session(
    aws_access_key_id=os.environ.get("tenant_aws_access_key_id", ""),
    aws_secret_access_key=os.environ.get("tenant_aws_secret_access_key", "")
)

def create_table():
    try:
        session.resource('dynamodb').create_table(
            TableName=table_name,
            AttributeDefinitions=[
                {
                    'AttributeName': 'name',
                    'AttributeType': 'S',
                },
                {
                    'AttributeName': 'api_key',
                    'AttributeType': 'S',
                }
            ],
            KeySchema=[
                {
                    'AttributeName': 'name',
                    'KeyType': 'HASH',
                },
                {
                    'AttributeName': 'api_key',
                    'KeyType': 'RANGE',
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5,
            }
        )
        table = session.resource('dynamodb').Table(
            table_name
        )
        table.wait_until_exists()
        return
    except ClientError:
        pass

table = session.resource('dynamodb').Table(
    table_name
)

def dynamo_to_dict(low_level_data):
    deserializer = TypeDeserializer()
    return {k: deserializer.deserialize(v) for k,v in low_level_data.items()}

def create_tenant(name: str, scope: list = ["*"]):
    try:
        existing_tenant = get_tenant_by_name(name)
        if existing_tenant:
            raise ExistingTenantException
    except TenantNotFoundException:
        pass
    api_key = str(uuid4())
    tenant_id = f"{str(uuid4())}-{name}"
    tenant_item = {
        'tenant_id': tenant_id,
        'name': name,
        'api_key': api_key,
        'scope': scope
    }
    response = table.put_item(
        Item=tenant_item
    )
    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
        raise TenantCreationException
    tenant = {
        "tenant_id": tenant_id,
        "name": name,
        "api_key": api_key,
        "scope": scope
    }
    return Tenant.parse_obj(tenant)

def get_tenant_by_name(name: str) -> Tenant or None:
    scan_kwargs = {
        'FilterExpression': Key('name').eq(name),
    }
    response = table.scan(
        **scan_kwargs
    )
    if response['Count'] == 0:
        raise TenantNotFoundException
    tenant = response['Items'][0]
    return Tenant.parse_obj(tenant)

def get_tenant(api_key: str, table_name=table_name) -> Tenant:
    table = session.resource('dynamodb').Table(
        table_name
    )
    scan_kwargs = {
        'FilterExpression': Key('api_key').eq(api_key),
    }
    response = table.scan(
        **scan_kwargs
    )
    if response['Count'] == 0:
        raise TenantNotFoundException
    tenant = response['Items'][0]
    return Tenant.parse_obj(tenant)

def get_project_name(apigw_id: str) -> str or None:
    apigw = boto3.client(
        'apigateway',
        aws_access_key_id=os.environ.get("tenant_aws_access_key_id", ""),
        aws_secret_access_key=os.environ.get("tenant_aws_secret_access_key", "")
    )
    try:
        response = apigw.get_rest_api(
            restApiId=apigw_id
        )
        project_name = response.get("name", "-").split("-")[0]
        return project_name
    except ClientError:
        return None