import os

stage = os.environ.get("stage_name", "/")
root_path=f"/{stage}" if stage[0] != "/" else stage
