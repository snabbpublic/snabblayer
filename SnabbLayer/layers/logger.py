import os
from aws_lambda_powertools import Logger
logger = Logger(service=os.environ.get('project_name', ""))

"""Log info messages
"""
def info(log_message):
    logger.info (log_message)

"""Log error messages
"""
def error(log_message):
    logger.error (log_message)

def log_with_tenant_context(tenant_id, log_message):
    logger.structure_logs(append=True, tenant_id=tenant_id)
    logger.info (log_message)