from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import APIKeyHeader
from functools import lru_cache
from .tenant_dal import create_tenant as create_tenant_dal, create_table, get_tenant as get_tenant_dal
from .models import ExistingTenantException, TenantRequest

api_key_header = APIKeyHeader(name="api-key")

router = APIRouter(
    prefix="/tenant",
)

@lru_cache()
def get_current_tenant(api_key: str = Depends(api_key_header)):
    return get_tenant_dal(api_key)

@router.post("/", include_in_schema=False)
async def create_tenant(tenant_request: TenantRequest, tenant=Depends(get_current_tenant)):
    try:
        create_table
        return create_tenant_dal(tenant_request.name, tenant_request.scope)
    except ExistingTenantException:
        raise HTTPException(status_code=400, detail="Tenant already exists")

@router.on_event('startup')
async def startup():
    try:
        create_table()
        admin_tenant = create_tenant_dal("admin", ["*", "tenant/*"])
        print(admin_tenant)
    except ExistingTenantException:
        pass